package me.themgrf.marketchests;

import me.themgrf.marketchests.commands.MarketChestCmd;
import com.gmail.filoghost.holographicdisplays.api.Hologram;
import me.themgrf.marketchests.listeners.BlockBreakListener;
import me.themgrf.marketchests.listeners.PlayerChatListener;
import me.themgrf.marketchests.listeners.PlayerInteractListener;
import me.themgrf.marketchests.listeners.SignChangeListener;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Chest;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;

public class Main extends JavaPlugin implements Listener {

    private static Main instance;
    private ShopLocations shopLocations = new ShopLocations();

    // Market Chest Sign Location | Hologram
    public HashMap<Location, Hologram> marketChests = new HashMap();

    public void onEnable() {
        instance = this;

        // Listeners
        PluginManager pm = getServer().getPluginManager();
        pm.registerEvents(new PlayerInteractListener(), this);
        pm.registerEvents(new PlayerChatListener(), this);
        pm.registerEvents(new SignChangeListener(), this);
        pm.registerEvents(new BlockBreakListener(), this);

        // Commands
        getCommand("marketchests").setExecutor(new MarketChestCmd());

        saveDefaultConfig();

        // Load individual shops and holograms into hashmap
        loadShops();
    }

    public void loadShops() {
        reloadConfig();

        // Remove holograms
        for (Hologram hologram : marketChests.values()) {
            hologram.delete();
        }

        if (getConfig().isSet("shops")) {
            if (shopLocations.getShopLocations().isEmpty() || shopLocations == null) {
                for (ShopLocation loc : shopLocations.getShopLocations()) {
                    if (getConfig().getString("shops." + loc.getWorld() + "." + loc.getCoords() + ".item") != null) {
                        try {
                            Location chestLoc = shopLocations.getLocation(loc);
                            Chest chest = (Chest) Utils.getAttachedShopChest(loc.getBukkitLocation().getBlock()).getBlock().getState();
                            Hologram hologram = Utils.spawnItem(Utils.getChestPrimaryItem(chest, Material.valueOf(getConfig().getString("shops." + loc.getWorld() + "." + loc.getCoords() + ".item"))), loc.getBukkitLocation(), Utils.getAttachedShopChest(loc.getBukkitLocation().getBlock()));
                            marketChests.put(loc.getBukkitLocation(), hologram);
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }

    /**
     * Get an instance of the main class
     * @return An instance of the main class
     */
    public static Main getInstance() {
        return instance;
    }

    /**
     * Get an instance of shop location methods
     * @return An instance of shop location methods
     */
    public ShopLocations getShopLocations() {
        return shopLocations;
    }
}