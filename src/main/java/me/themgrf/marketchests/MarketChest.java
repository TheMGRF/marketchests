package me.themgrf.marketchests;

public class MarketChest {

    private ShopLocation shopLocation;
    private String owner, item;
    private double price;

    public MarketChest(ShopLocation shopLocation, String owner, String item, double price) {
        this.shopLocation = shopLocation;
        this.owner = owner;
        this.item = item;
        this.price = price;
    }

    public ShopLocation getShopLocation() {
        return shopLocation;
    }

    public String getOwner() {
        return owner;
    }

    public String getItem() {
        return item;
    }

    public double getPrice() {
        return price;
    }
}
