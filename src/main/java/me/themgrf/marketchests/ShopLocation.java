package me.themgrf.marketchests;

import org.bukkit.Bukkit;
import org.bukkit.Location;

public class ShopLocation {

    private String world, coords;

    public ShopLocation(Location loc) {
        this.world = loc.getWorld().getName();
        this.coords = (int) loc.getX() + "," + (int) loc.getY() + "," + (int) loc.getZ();
    }

    public String getWorld() {
        return world;
    }

    public String getCoords() {
        return coords;
    }

    public Location getBukkitLocation() {
        String[] coords = this.coords.split(",");

        return new Location(Bukkit.getWorld(world),
                Double.parseDouble(coords[0]),
                Double.parseDouble(coords[1]),
                Double.parseDouble(coords[2]));
    }

}
