package me.themgrf.marketchests;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class ShopLocations {

    public void saveShops(Player player, Location loc, Material item, double price) {
        String path = "shops."+ "." + loc.getWorld().getName() + "." + (int) loc.getX() + "," + (int) loc.getY() + "," + (int) loc.getZ();
        Main.getInstance().getConfig().set(path + ".owner", player.getUniqueId().toString());
        Main.getInstance().getConfig().set(path + ".item", item.name());
        Main.getInstance().getConfig().set(path + ".price", price);
        Main.getInstance().saveConfig();
    }

    public ArrayList<ShopLocation> getShopLocations() {
        ArrayList<ShopLocation> shops = new ArrayList<>();

        for (String world : Main.getInstance().getConfig().getConfigurationSection("shops").getKeys(false)) {
            for (String loc : Main.getInstance().getConfig().getConfigurationSection("shops." + world).getKeys(false)) {
                String[] temp = loc.split(",");

                shops.add(new ShopLocation(new Location(Bukkit.getWorld(world), Integer.parseInt(temp[0]), Integer.parseInt(temp[1]), Integer.parseInt(temp[2]))));
            }
        }

        return shops;
    }

    public Location getLocation(ShopLocation location) {
        return location.getBukkitLocation();
    }

    public Material getItem(Player player, Location loc) {
        return Material.valueOf(Main.getInstance().getConfig().getString("shops." + player.getUniqueId().toString() + "." + (int) loc.getX() + "," + (int) loc.getY() + "," + (int) loc.getZ() + ".item"));
    }
}

