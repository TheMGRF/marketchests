package me.themgrf.marketchests;

import com.earth2me.essentials.api.Economy;
import com.earth2me.essentials.api.NoLoanPermittedException;
import com.earth2me.essentials.api.UserDoesNotExistException;
import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;
import com.gmail.filoghost.holographicdisplays.api.line.ItemLine;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.block.Sign;
import org.bukkit.block.data.type.WallSign;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.math.BigDecimal;
import java.util.ArrayList;

public class Utils {

    /**
     * Get a players economy balance
     * @param player The player who's balance should be fetched
     * @return The players current economy balance
     */
    public static BigDecimal getBalance(String player) {
        BigDecimal money;
        try {
            money = Economy.getMoneyExact(player);
        } catch (UserDoesNotExistException e) {
            e.printStackTrace();
            return BigDecimal.valueOf(-1);
        }

        return money;
    }

    /**
     * Check to see if the player has enough money
     * @param player The player who's balance should be fetched
     * @param amount The amount of money that the player's balance should be compared to
     * @return <code>true</code> if player has enough money; <code>false</code> if the player does not have enough money
     */
    public static boolean hasEnoughMoney(String player, double amount) {
        if (getBalance(player).compareTo(BigDecimal.valueOf(amount)) >= 0) {
            return true;
        }
        return false;
    }

    /**
     * Take away money from a player's account
     * @param player The player who's account should be used
     * @param amount The amount to take from the player's account
     * @return <code>true</code> if it successfully takes money; <code>false</code> if it fails to take money
     */
    public static boolean takeMoney(String player, double amount) {
        try {
            Economy.substract(player, BigDecimal.valueOf(amount));
            return true;
        } catch (UserDoesNotExistException | NoLoanPermittedException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Give a player money
     * @param player The player who's account should be used
     * @param amount The amount to give the player
     * @return <code>true</code> if it successfully adds money; <code>false</code> if it fails to add money
     */
    public static boolean giveMoney(String player, double amount) {
        try {
            Economy.add(player, BigDecimal.valueOf(amount));
            return true;
        } catch (UserDoesNotExistException | NoLoanPermittedException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Get a market chest by location
     * @param loc The location to check for a market chest
     * @return The market chest at the location, or <code>null</code> if there is no market chest found
     */
    public static MarketChest getMarketChest(Location loc) {
        String path = "shops." + loc.getWorld().getName() + "." + (int) loc.getX() + "," + (int) loc.getY() + "," + (int) loc.getZ();

        if (Main.getInstance().getConfig().getConfigurationSection(path) != null) {
            ConfigurationSection cs = Main.getInstance().getConfig().getConfigurationSection(path);
            return new MarketChest(new ShopLocation(loc), cs.getString("owner"), cs.getString("item"), cs.getDouble("price"));
        }

        return null;
    }

    /**
     * Spawn an item (hologram) above a market chest
     * @param item The item of the chest
     * @param origin The origin location of the market chest (sign)
     * @param loc The location of the market chest
     * @return The item hologram
     */
    public static Hologram spawnItem(ItemStack item, Location origin, Location loc) {
        loc = loc.add(0.5, 1.7, 0.5);

        final Hologram hologram = HologramsAPI.createHologram(Main.getInstance(), loc);
        //item.setAmount(1);
        ItemLine itemLine = hologram.appendItemLine(item);

        Main.getInstance().marketChests.put(origin, hologram);

        return hologram;
    }

    /**
     *
     * @param chest
     * @return
     */
    public static ItemStack getChestPrimaryItem(Chest chest, Material backup) {
        for (ItemStack item : chest.getBlockInventory().getContents()) {
            if (item != null) {
                ItemStack returnItem = item.clone();
                return returnItem;
            }
        }
        return new ItemStack(backup, 1);
    }

    /**
     * Convert a material value to a readable string
     * @param material The material value to convert
     * @return The readable string value of the material
     */
    public static String convertItemMaterial(Material material) {

        String convert = material.toString().toLowerCase().replaceAll("\\_", " ");
        String newName = "";

        boolean space = true;
        for (int i = 0; i < convert.length(); i++) {
            if (space) newName += Character.toUpperCase(convert.charAt(i));
            else newName += convert.charAt(i);
            space = convert.charAt(i) == ' ';
        }

        return newName;
    }

    /**
     * Remove items from a market chest
     * @param chest The chest to remove the items from
     * @param type The material value of the item to remove
     * @param amount The amount of the item to remove
     */
    public static void removeInventoryItems(Chest chest, Material type, int amount, Player player) {
        Inventory inv = chest.getBlockInventory();
        for (ItemStack is : inv.getContents()) {
            if (is != null && is.getType() == type) {

                int newamount = is.getAmount() - amount;
                if (newamount > 0) {
                    ItemStack give = is;
                    give.setAmount(amount);
                    player.getInventory().addItem(give);

                    is.setAmount(newamount);
                    break;
                } else {
                    player.getInventory().addItem(is);
                    is.setAmount(0);
                    amount = -newamount;
                    if (amount == 0) break;
                }
            }
        }
    }

    /**
     * Get the items to remove from the market chest
     * @param chest The chest to remove the items from
     * @param type The material value of the item to remove
     * @param amount The amount of the item to remove
     * @return The items to remove from the market chest
     *
     * @deprecated Deprecated due to redundancy
     */
    @Deprecated
    public static ArrayList<ItemStack> getItemsToRemove(Chest chest, Material type, int amount) {
        ArrayList<ItemStack> items = new ArrayList<>();

        Inventory inv = chest.getBlockInventory();
        int loop = 0;
        for (ItemStack is : inv.getContents()) {
            if (is != null && is.getType() == type) {
                if (loop < amount) {
                    loop++;
                    items.add(is);
                }
            }
        }

        return items;
    }

    /**
     * Check to see if a block is a market chest sign shop
     * @param block The block to check
     * @return <code>true</code> if the block is a market chest; <code>false</code> if the block is not a market chest
     */
    public static boolean isShopChest(Block block) {
        if (block.getType() == Material.CHEST) {
            if (isSign(block.getRelative(-1, 0, 0))) {
                return true;
            } else if (isSign(block.getRelative(1, 0, 0))) {
                return true;
            } else if (isSign(block.getRelative(0, 0, -1))) {
                return true;
            } else if (isSign(block.getRelative(0, 0, 1))) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check to see if a block is a shop sign
     * @param block The block to check
     * @return <code>true</code> if the block is a shop sign; <code>false</code> if the block is not a shop chest
     */
    private static boolean isSign(Block block) {
        if (block.getState() instanceof Sign || block.getState() instanceof WallSign) {
            Sign sign = (Sign) block.getState();
            if (sign.getLine(0).equals("§1[SHOP]")) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get the shop sign attached to a market chest
     * @param block The block to check around
     * @return The location of the attached shop sign
     */
    public static Location getAttachedShopSign(Block block) {
        if (isSign(block.getRelative(-1, 0, 0))) {
            return block.getRelative(-1, 0, 0).getLocation();
        } else if (isSign(block.getRelative(1, 0, 0))) {
            return block.getRelative(1, 0, 0).getLocation();
        } else if (isSign(block.getRelative(0, 0, -1))) {
            return block.getRelative(0, 0, -1).getLocation();
        } else if (isSign(block.getRelative(0, 0, 1))) {
            return block.getRelative(0, 0, 1).getLocation();
        }
        return null;
    }

    /**
     * Get the market chest that the shop sign attached to
     * @param block The block to check around
     * @return The location of the market chest the sign is attached to
     */
    public static Location getAttachedShopChest(Block block) {
        if (isShopChest(block.getRelative(-1, 0, 0))) {
            return block.getRelative(-1, 0, 0).getLocation();
        } else if (isShopChest(block.getRelative(1, 0, 0))) {
            return block.getRelative(1, 0, 0).getLocation();
        } else if (isShopChest(block.getRelative(0, 0, -1))) {
            return block.getRelative(0, 0, -1).getLocation();
        } else if (isShopChest(block.getRelative(0, 0, 1))) {
            return block.getRelative(0, 0, 1).getLocation();
        }
        return null;
    }

    /**
     * Format a string to add colour codes
     * @param msg The string to format
     * @return The formatted, colour coded string.
     */
    public static String format(String msg) {
        return ChatColor.translateAlternateColorCodes('&', msg);
    }
}
