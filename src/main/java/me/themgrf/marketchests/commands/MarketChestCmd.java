package me.themgrf.marketchests.commands;

import me.themgrf.marketchests.Main;
import me.themgrf.marketchests.Utils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class MarketChestCmd implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] arg) {
        if (sender.hasPermission("darks.command.marketchests")) {
            Main.getInstance().loadShops();
            sender.sendMessage(Utils.format("&2&l(!) &aReloaded Market Chests Config!"));
        } else {
            sender.sendMessage(Utils.format("&cYou do not have access to this command!"));
        }
        return true;
    }
}
