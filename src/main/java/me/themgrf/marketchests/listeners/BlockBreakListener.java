package me.themgrf.marketchests.listeners;

import me.themgrf.marketchests.Main;
import me.themgrf.marketchests.MarketChest;
import me.themgrf.marketchests.Utils;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class BlockBreakListener implements Listener {

    @EventHandler
    public void onBreak(BlockBreakEvent e) {
        Block block = e.getBlock();

        if (Utils.isShopChest(block)) {
            e.setCancelled(true);
            Player player = e.getPlayer();

            player.sendMessage(Utils.format("&4&l(!) &cYou cannot break this chest because it is attached to a market shop!"));
            player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_BASS, 0.1f, 1);
            return;
        }

        BlockState state = block.getState();
        if (state instanceof Sign) {
            Sign sign = (Sign) state;
            if (sign.getLine(0).equals(Utils.format("&1[SHOP]"))) {
                Location loc = block.getLocation();
                MarketChest marketChest = Utils.getMarketChest(loc);
                Player player = e.getPlayer();
                if (marketChest != null && marketChest.getOwner().equals(player.getUniqueId().toString())) {
                    if (player.isSneaking()) {
                        // break shop
                        String path = "shops."+ "." + loc.getWorld().getName() + "." + (int) loc.getX() + "," + (int) loc.getY() + "," + (int) loc.getZ();

                        if (Main.getInstance().marketChests.containsKey(sign.getLocation())) {
                            Main.getInstance().marketChests.get(sign.getLocation()).delete();
                            Main.getInstance().marketChests.remove(sign.getLocation());

                            Main.getInstance().getConfig().set(path, null);
                            Main.getInstance().saveConfig();

                            player.playSound(player.getLocation(), Sound.ENTITY_CHICKEN_EGG, 1, 1);
                            player.sendMessage(Utils.format("&2&l(!) &aChest shop removed!"));
                            player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_PLING, 1, 1);

                            return;
                        }
                    } else {
                        player.sendMessage(Utils.format("&4&l(!) &cTo remove this shop please sneak and break this sign!"));
                        e.setCancelled(true);
                    }
                } else {
                    if (sign.getLine(3).equals(player.getName())) {
                        String path = "shops."+ "." + loc.getWorld().getName() + "." + (int) loc.getX() + "," + (int) loc.getY() + "," + (int) loc.getZ();

                        if (Main.getInstance().marketChests.containsKey(sign.getLocation())) {
                            Main.getInstance().marketChests.get(sign.getLocation()).delete();
                            Main.getInstance().marketChests.remove(sign.getLocation());

                            Main.getInstance().getConfig().set(path, null);
                            Main.getInstance().saveConfig();

                            player.playSound(player.getLocation(), Sound.ENTITY_CHICKEN_EGG, 1, 1);
                            player.sendMessage(Utils.format("&2&l(!) &aChest shop removed!"));
                            player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_PLING, 1, 1);

                            return;
                        }
                    } else {
                        if (player.hasPermission("darks.marketchest.override")) {
                            if (player.isSneaking()) {
                                String path = "shops." + "." + loc.getWorld().getName() + "." + (int) loc.getX() + "," + (int) loc.getY() + "," + (int) loc.getZ();
                                if (Main.getInstance().marketChests.containsKey(sign.getLocation())) {
                                    Main.getInstance().marketChests.get(sign.getLocation()).delete();
                                    Main.getInstance().marketChests.remove(sign.getLocation());

                                    Main.getInstance().getConfig().set(path, null);
                                    Main.getInstance().saveConfig();

                                    player.playSound(player.getLocation(), Sound.ENTITY_CHICKEN_EGG, 1, 1);
                                    player.sendMessage(Utils.format("&2&l(!) &aRemoved " + sign.getLine(3) + " chest shop!"));
                                    player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_PLING, 1, 1);

                                    return;
                                }
                            } else {
                                player.sendMessage(Utils.format("&4&l(!) &cYou need to sneak to remove other peoples shops!"));
                                e.setCancelled(true);
                            }
                        } else {
                            player.sendMessage(Utils.format("&4&l(!) &cYou cannot remove other peoples shops!"));
                            e.setCancelled(true);
                        }
                    }
                }
            }
        }
    }
}
