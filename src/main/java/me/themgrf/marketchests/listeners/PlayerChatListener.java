package me.themgrf.marketchests.listeners;

import me.themgrf.marketchests.Main;
import me.themgrf.marketchests.MarketChest;
import me.themgrf.marketchests.Utils;
import org.apache.commons.lang.math.NumberUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Chest;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.inventory.ItemStack;

import java.math.BigDecimal;
import java.util.UUID;

public class PlayerChatListener implements Listener {

    @EventHandler
    public void chatListener(AsyncPlayerChatEvent e) {
        Player player = e.getPlayer();
        if (player.hasMetadata("marketChest")) {
            e.setCancelled(true);

            if (e.getMessage().equals("cancel")) {
                e.setCancelled(true);
                player.removeMetadata("marketChest", Main.getInstance());
                player.removeMetadata("marketChestBlock", Main.getInstance());

                player.sendMessage(Utils.format("&4&l(!) &cPurchase Cancelled!"));
                player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_PLING, 1, 1);
                return;
            }
            if (NumberUtils.isNumber(e.getMessage())) {
                String pQuantity = e.getMessage();
                int quantity = Integer.parseInt(pQuantity);

                if (quantity > 0) {
                    e.setCancelled(true);

                    Chest chest = (Chest) player.getMetadata("marketChestBlock").get(0).value();
                    MarketChest marketChest = (MarketChest) player.getMetadata("marketChest").get(0).value();
                    Material material = Material.valueOf(marketChest.getItem());

                    if (buyItem(chest, marketChest, Integer.parseInt(e.getMessage()))) {
                        // chest has enough items
                        double cost =  marketChest.getPrice() * quantity;
                        if (Utils.hasEnoughMoney(player.getName(), cost)) {
                            if (Utils.takeMoney(player.getName(), cost)) {
                                String target = Bukkit.getOfflinePlayer(UUID.fromString(marketChest.getOwner())).getName();
                                if (Utils.giveMoney(target, cost)) {
                                    Utils.removeInventoryItems(chest, material, quantity, player);

                                    //player.getInventory().addItem(new ItemStack(material, quantity));

                                    if (Bukkit.getPlayer(target) != null) { Bukkit.getPlayer(target).sendMessage(Utils.format("&6&l(!) &a" + player.getName() + " &ejust bought &f" + quantity + " " + Utils.convertItemMaterial(material) + " &efrom one of your shops!")); }
                                    player.sendMessage(Utils.format("&2&l(!) &aYou successfully bought &e" + quantity + " &f" + Utils.convertItemMaterial(material) + " &afrom &6" + target + " &afor &2$&a" + cost));
                                    player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_CHIME, 1, 1);

                                    player.removeMetadata("marketChest", Main.getInstance());
                                    player.removeMetadata("marketChestBlock", Main.getInstance());
                                    return;
                                } else {
                                    player.sendMessage(Utils.format("&4&l(!) §cAn error occured when trying to purchase this item, please try again!"));
                                    player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_BASS, 1, 1);
                                }

                            } else {
                                player.sendMessage(Utils.format("&4&l(!) §cAn error occured when trying to purchase this item, please try again!"));
                                player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_BASS, 1, 1);
                            }
                        } else {
                            player.sendMessage(Utils.format("&4&l(!) §cYou do not have enough money to buy that much! You need &2$&a" + BigDecimal.valueOf(cost).subtract(Utils.getBalance(player.getName())) + " &cmore!"));
                            player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_BASS, 1, 1);
                        }
                    } else {
                        // shop doesnt have that many items
                        int count = countContents(chest, material);
                        if (count > 0) {
                            player.sendMessage(Utils.format("&4&l(!) &cThis shop only has &e" + count + " &f" + Utils.convertItemMaterial(material) + " &cremaining!"));
                        } else {
                            player.sendMessage(Utils.format("&4&l(!) &cThis shop has no &f" + Utils.convertItemMaterial(material) + " &cremaining!"));
                        }

                        player.removeMetadata("marketChest", Main.getInstance());
                        player.removeMetadata("marketChestBlock", Main.getInstance());
                        return;
                    }

                    player.removeMetadata("marketChest", Main.getInstance());
                    player.removeMetadata("marketChestBlock", Main.getInstance());
                    return;
                }
            }
            player.sendMessage(Utils.format("&4&l(!) &cPlease enter a number or type \"&7cancel&c\" to cancel purchase!"));
        }
    }

    private boolean buyItem(Chest chest, MarketChest marketChest, int quantity) {
        if (chest.getBlockInventory().contains(Material.valueOf(marketChest.getItem()), quantity)) {
            return true;
        }
        return false;
    }

    private int countContents(Chest chest, Material material) {
        int total = 0;
        for (ItemStack item : chest.getBlockInventory().getContents()) {
            if (item != null) {
                if (item.getType() == material) {
                    total += item.getAmount();
                }
            }
        }

        return total;
    }
}


