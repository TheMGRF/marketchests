package me.themgrf.marketchests.listeners;

import me.themgrf.marketchests.Main;
import me.themgrf.marketchests.MarketChest;
import me.themgrf.marketchests.Utils;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.*;
import org.bukkit.block.data.type.WallSign;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.metadata.FixedMetadataValue;

public class PlayerInteractListener implements Listener {

    @EventHandler
    public void SignClick(PlayerInteractEvent e) {
        Action action = e.getAction();

        if (action.equals(Action.RIGHT_CLICK_BLOCK)) {
            Player player = e.getPlayer();
            Block block = e.getClickedBlock();
            BlockState state = block.getState();

            if (block.getType() == Material.CHEST) {
                if (Utils.isShopChest(block)) {
                    if (Utils.getMarketChest(Utils.getAttachedShopSign(block)) != null && Utils.getMarketChest(Utils.getAttachedShopSign(block)).getOwner() != null) {
                        if (!Utils.getMarketChest(Utils.getAttachedShopSign(block)).getOwner().equals(player.getUniqueId().toString())) {
                            if (player.hasPermission("darks.marketchest.override")) {
                                if (player.isSneaking()) {
                                    return;
                                } else {
                                    e.setCancelled(true);
                                    player.playSound(player.getLocation(), Sound.BLOCK_CHEST_LOCKED, 1, 1);
                                    player.sendMessage(Utils.format("&4&l(!) &cYou must be sneaking to open other peoples Market Chests!"));
                                    return;
                                }
                            }

                            e.setCancelled(true);
                            player.playSound(player.getLocation(), Sound.BLOCK_CHEST_LOCKED, 1, 1);
                            player.sendMessage(Utils.format("&4&l(!) &cYou cant open this shop as it belongs to someone else!"));
                        }
                    }
                }
                return;
            }

            if (state instanceof Sign) {
                Sign sign = (Sign) state;
                WallSign wallSign = (WallSign) sign.getBlock().getState().getBlockData();
                BlockFace blockFace = wallSign.getFacing().getOppositeFace();

                Block attached = sign.getBlock().getRelative(blockFace);
                String line0 = sign.getLine(0);

                if (attached.getType() == Material.CHEST) {
                    if (line0.equals("§1[SHOP]")) {
                        String itemLine = sign.getLine(2);
                        String moneyLine = sign.getLine(1);
                        String nameLine = sign.getLine(3);

                        Location loc = sign.getLocation();

                        if (Utils.getMarketChest(loc) != null && Utils.getMarketChest(loc).getOwner() != null) {
                            if (Utils.getMarketChest(loc).getOwner().equals(player.getUniqueId().toString())) {
                                // player owns this shop
                                sign.setLine(3, player.getName()); // Just make sure the name is updated

                                if (itemLine.length() == 0) {
                                    String pItem = player.getInventory().getItemInMainHand().getType().name();
                                    if (pItem.length() > 0) {
                                        if (pItem.equals("AIR")) {
                                            player.sendMessage(Utils.format("&6&l(!) &eClick on the sign with the item you want to sell!"));
                                            return;
                                        } else {
                                            sign.setLine(2, Utils.convertItemMaterial(Material.valueOf(pItem)));
                                            sign.update();

                                            double price = Double.parseDouble(moneyLine.replace("§2$§a", ""));
                                            Main.getInstance().getShopLocations().saveShops(player, sign.getLocation(), Material.valueOf(pItem), price);
                                            Utils.spawnItem(player.getInventory().getItemInMainHand(), sign.getLocation(), attached.getLocation());
                                            return;
                                        }
                                    }
                                }

                                player.sendMessage(Utils.format("&4&l(!) &cYou own this shop and cannot buy from it!"));
                            } else {
                                // player doesnt own shop
                                if (Utils.getMarketChest(loc) != null) {
                                    if (!player.hasMetadata("marketChest")) {
                                        MarketChest marketChest = Utils.getMarketChest(loc);

                                        player.sendMessage(Utils.format("&6&l(!) &eType into chat the amount of &f" + Utils.convertItemMaterial(Material.valueOf(marketChest.getItem())) + " &eyou wish to buy or type \"&7cancel&e\" to cancel purchase!"));
                                        player.setMetadata("marketChest", new FixedMetadataValue(Main.getInstance(), marketChest));

                                        Chest chest = (Chest) attached.getState();
                                        player.setMetadata("marketChestBlock", new FixedMetadataValue(Main.getInstance(), chest));
                                    }
                                }
                            }
                        }
                    }
                } else {
                    if ("[shop]".equals(line0)) {
                        player.sendMessage(Utils.format("&4&l(!) &cPlace this sign on a chest if you want to sell items."));
                    }
                }
            }
        }
    }
}


