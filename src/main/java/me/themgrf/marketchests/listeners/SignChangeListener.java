package me.themgrf.marketchests.listeners;

import me.themgrf.marketchests.Main;
import me.themgrf.marketchests.ShopLocation;
import me.themgrf.marketchests.Utils;
import org.apache.commons.lang.math.NumberUtils;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.WallSign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;

public class SignChangeListener implements Listener {

    @EventHandler
    public void onSignEdit(SignChangeEvent e) {
        BlockState state = e.getBlock().getState();

        if (state instanceof Sign) {
            Sign sign = (Sign) state;
            WallSign wallSign = (WallSign) sign.getBlock().getState().getBlockData();
            BlockFace blockFace = wallSign.getFacing().getOppositeFace();

            Block attached = sign.getBlock().getRelative(blockFace);

            Player player = e.getPlayer();

            if (attached.getType() == Material.CHEST) {
                if (e.getLine(0).equalsIgnoreCase("[shop]")) {
                    if (!e.getLine(1).isEmpty()) {
                        if (NumberUtils.isNumber(e.getLine(1))) {
                            e.setLine(0, Utils.format("&1[SHOP]"));
                            e.setLine(1, Utils.format("&2$&a" + e.getLine(1).replace("$", "").replace("£", "").replace("€", "")));
                            e.setLine(2, "");
                            e.setLine(3, player.getName());

                            ShopLocation shopLocation = new ShopLocation(sign.getLocation());

                            Main.getInstance().getConfig().set("shops." + shopLocation.getWorld() + "." + shopLocation.getCoords() + ".owner", player.getUniqueId().toString());
                            Main.getInstance().getConfig().set("shops." + shopLocation.getWorld() + "." + shopLocation.getCoords() + ".price", Double.parseDouble(e.getLine(1).replace("§2$§a", "")));
                            Main.getInstance().saveConfig();

                            player.sendMessage(Utils.format("&6&l(!) &eTo finish creating your market chest click the sign with the item you would like to sell!"));
                        } else {
                            e.getBlock().breakNaturally();
                            player.playSound(player.getLocation(), Sound.ENTITY_CHICKEN_EGG, 1, 1);
                            player.sendMessage(Utils.format("&4&l(!) &cPlease provide a valid price on line 2! E.g $15"));
                        }
                    } else {
                        e.getBlock().breakNaturally();
                        player.playSound(player.getLocation(), Sound.ENTITY_CHICKEN_EGG, 1, 1);
                        player.sendMessage(Utils.format("&4&l(!) &cPlease provide a price on line 2!"));
                    }
                }
            } else {
                if (e.getLine(0).equalsIgnoreCase("[shop]")) {
                    sign.getBlock().breakNaturally();
                    player.playSound(player.getLocation(), Sound.ENTITY_CHICKEN_EGG, 1, 1);
                    player.sendMessage(Utils.format("&4&l(!) &cPlace this sign on a chest if you want to sell items."));
                    player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_BASS, 1, 1);
                }
            }
        }
    }

}
